/*
    A mapping table that maps key codes (including modifier keys) to their ASCII values. 
    This table is for a generic US keyboard. For other languages, a new mapping table 
    will need to be setup.
*/


unsigned char keyMap[][4] =
{
    // plain,   shift down, ctrl down,  shift&ctrl
    0xFF,       0xFF,       0xFF,       0xFF,       //  0   $00 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       //  1   $01 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       //  2   $02 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       //  3   $03 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       //  4   $04 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       //  5   $05 ;   NA
    0x03,       0xFF,       0xFF,       0xFF,       //  6   $06 ;   Break
    0xFF,       0xFF,       0xFF,       0xFF,       //  7   $07 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       //  8   $08 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       //  9   $09 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 10   $0A ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 11   $0B ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 12   $0C ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 13   $0D ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 14   $0E ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 15   $0F ;   NA
    
    0xFF,       0xFF,       0xFF,       0xFF,       // 16   $10 ;   NA
    0xDC,       0xDC,       0xDC,       0xDC,       // 17   $11 ;   Home
    0xDD,       0xDD,       0xDD,       0xDD,       // 18   $12 ;   End
    0x13,       0xFF,       0x11,       0xFF,       // 19   $13 ;   Page Up
    0x14,       0xFF,       0x12,       0xFF,       // 20   $14 ;   Page Down
    0x13,       0xFF,       0x01,       0xFF,       // 21   $15 ;   Cursor Left
    0x04,       0xFF,       0x06,       0xFF,       // 22   $16 ;   Cursor Right
    0x05,       0xFF,       0x12,       0xFF,       // 23   $17 ;   Cursor Up
    0x18,       0xFF,       0x03,       0xFF,       // 24   $18 ;   Cursor Down
    0x16,       0xFF,       0xFF,       0xFF,       // 25   $19 ;   Insert
    0x7F,       0xFF,       0xFF,       0xFF,       // 26   $1A ;   Delete
    0x1B,       0xFF,       0xFF,       0xFF,       // 27   $1B ;   Escape
    0x08,       0xFF,       0xFF,       0xFF,       // 28   $1C ;   Backspace
    0x09,       0xFF,       0xFF,       0xFF,       // 29   $1D ;   TAB
    0x0D,       0xFF,       0xFF,       0xFF,       // 30   $1E ;   Return
    ' ',        ' ',        ' ',        ' ',        // 31   $1F ;   Space

    0xFF,       0xFF,       0xFF,       0xFF,       // 32   $20 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 33   $21 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 34   $22 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 35   $23 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 36   $24 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 37   $25 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 38   $26 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 39   $27 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 40   $28 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 41   $29 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 42   $2A ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 43   $2B ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 44   $2C ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 45   $2D ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 46   $2E ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 47   $2F ;   NA
    
    '0',        ')',        0xFF,       0xFF,       // 48   $30 ;   0 - )
    '1',        '!',        0xFF,       0xFF,       // 49   $31 ;   1 - !
    '2',        '@',        0xFF,       0xFF,       // 50   $32 ;   2 - @
    '3',        '#',        0xFF,       0xFF,       // 51   $33 ;   3 - #
    '4',        '$',        0xFF,       0xFF,       // 52   $34 ;   4 - $
    '5',        '%',        0xFF,       0xFF,       // 53   $35 ;   5 - %
    '6',        '^',        0xFF,       0xFF,       // 54   $36 ;   6 - ^
    '7',        '&',        0xFF,       0xFF,       // 55   $37 ;   7 - &
    '8',        '*',        0xFF,       0xFF,       // 56   $38 ;   8 - *
    '9',        '(',        0xFF,       0xFF,       // 57   $39 ;   9 - (
    '\'',       '"',        0x3A,       0x3A,       // 58   $3A ;   : - )n
    ',',        '<',        0x3B,       0x3B,       // 59   $3B ;   ; - <
    '-',        '_',        0x3C,       0x3C,       // 60   $3C ;   < - _
    '.',        '>',        0x3D,       0x3D,       // 61   $3D ;   = - >
    '/',        '?',        0x3E,       0x3E,       // 62   $3E ;   > - ?
    0xFF,       0xFF,       0xFF,       0xFF,       // 63   $3F ;   NA

    '`',        '~',        0xFF,       0xFF,       // 64   $40 ;   ` - ~
    'a',        'A',        0x01,       0xFF,       // 65   $41 ;   a - A - ctrl a
    'b',        'B',        0x02,       0xFF,       // 66   $42 ;   b - B - ctrl b
    'c',        'C',        0x03,       0xFF,       // 67   $43 ;   c - C - ctrl c
    'd',        'D',        0x04,       0xFF,       // 68   $44 ;   d - D - ctrl d
    'e',        'E',        0x05,       0xFF,       // 69   $45 ;   e - E - ctrl e
    'f',        'F',        0x06,       0xFF,       // 70   $46 ;   f - F - ctrl f
    'g',        'G',        0x07,       0xFF,       // 71   $47 ;   g - G - ctrl g
    'h',        'H',        0x08,       0xFF,       // 72   $48 ;   h - H - ctrl h
    'i',        'I',        0x09,       0xFF,       // 73   $49 ;   i - I - ctrl i
    'j',        'J',        0x0A,       0xFF,       // 74   $4A ;   j - J - ctrl j
    'k',        'K',        0x0B,       0xFF,       // 75   $4B ;   k - K - ctrl k
    'l',        'L',        0x0C,       0xFF,       // 76   $4C ;   l - L - ctrl l
    'm',        'M',        0x0D,       0xFF,       // 77	$4D ;   m - M - ctrl m
    'n',        'N',        0x0E,       0xFF,       // 78	$4E ;   n - N - ctrl n
    'o',        'O',        0x0F,       0xFF,       // 79	$4F ;   o - O - ctrl o

    'p',        'P',        0x10,       0x10,       // 80	$50 ;   p - P - ctrl p
    'q',        'Q',        0x11,       0x11,       // 81	$51 ;   q - Q - ctrl q
    'r',        'R',        0x12,       0x12,       // 82	$52 ;   r - R - ctrl r
    's',        'S',        0x13,       0x13,       // 83	$53 ;   s - S - ctrl s
    't',        'T',        0x14,       0x14,       // 84	$54 ;   t - T - ctrl t
    'u',        'U',        0x15,       0x15,       // 85	$55 ;   u - U - ctrl u
    'v',        'V',        0x16,       0x16,       // 86	$56 ;   v - V - ctrl v
    'w',        'W',        0x17,       0x17,       // 87	$57 ;   w - W - ctrl w
    'x',        'X',        0x18,       0x18,       // 88	$58 ;   x - X - ctrl x
    'y',        'Y',        0x19,       0x19,       // 89	$59 ;   y - Y - ctrl y
    'z',        'Z',        0x1A,       0x1A,       // 90	$5A ;   z - Z - ctrl z
    ';',        ':',        0xFF,       0xFF,       // 91	$5B ;   ; - :
    '\\',       '|',        0xFF,       0xFF,       // 92	$5C ;   \ - |
    '[',        '{',        0xFF,       0xFF,       // 93	$5D ;   [ - {
    ']',        '}',        0xFF,       0xFF,       // 94	$5E ;   ] - }
    '=',        '+',        0xFF,       0xFF,       // 95	$5F ;   = - +

    0xFF,       0xFF,       0xFF,       0xFF,       // 96	$60 ;   NA
    0xD0,       0xE0,       0xF0,       0xFF,       // 97	$61 ;   F1
    0xD1,       0xE1,       0xF1,       0xFF,       // 98	$62 ;   F2
    0xD2,       0xE2,       0xF2,       0xFF,       // 99	$63 ;   F3
    0xD3,       0xE3,       0xF3,       0xFF,       // 100	$64 ;   F4
    0xD4,       0xE4,       0xF4,       0xFF,       // 101	$65 ;   F5
    0xD5,       0xE5,       0xF5,       0xFF,       // 102	$66 ;   F6
    0xD6,       0xE6,       0xF6,       0xFF,       // 103	$67 ;   F7
    0xD7,       0xE7,       0xF7,       0xFF,       // 104	$68 ;   F8
    0xD8,       0xE8,       0xF8,       0xFF,       // 105	$69 ;   F9
    0xD9,       0xE9,       0xF9,       0xFF,       // 106	$6A ;   F10
    0xDA,       0xEA,       0xFA,       0xFF,       // 107	$6B ;   F11
    0xDB,       0xEB,       0xFB,       0xFF,       // 108	$6C ;   F12
    0xFF,       0xFF,       0xFF,       0xFF,       // 109	$6D ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 110	$6E ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 111	$6F ;   NA

    0xFF,       0xFF,       0xFF,       0xFF,       // 112	$70 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 113	$71 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 114	$72 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 115	$73 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 116	$74 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 117	$75 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 118	$76 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 119	$77 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 120	$78 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 121	$79 ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 122	$7A ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 123	$7B ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 124	$7C ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 125	$7D ;   NA
    0xFF,       0xFF,       0xFF,       0xFF,       // 126	$7E ;   NA
    0xFF,       0xFF,       0xFF,       0xFF        // 127	$7F ;   NA
};    
